package kz.jusan.lessonone

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //findViewById<TextView>(R.id.testTextView).text = "Changed from code"
        val helloTextView: TextView = findViewById(R.id.testTextView)
        helloTextView.text = getString(R.string.title_salem)
        helloTextView.setOnClickListener {
            println("click")
        }
    }
}